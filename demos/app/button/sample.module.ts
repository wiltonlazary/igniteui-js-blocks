import { NgModule } from "@angular/core";
import { IgxDirectivesModule } from "../../../src/main";
import { ButtonsSampleComponent } from "./sample.component";

@NgModule({
    declarations: [ButtonsSampleComponent],
    imports: [IgxDirectivesModule]
})
export class ButtonSampleModule {}
